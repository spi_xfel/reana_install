# Get json of CWL workflow with "$graph" and substitute smth 

def jstr_get_wfs(json_content):
    result={}
    for wf in json_content['$graph']:
        result[wf['id']]=wf
    return result

def jstr_substitute(obj, wfs):
    if isinstance(obj, dict):
        for k in obj.keys():
            obj[k]=jstr_substitute(obj[k], wfs)
    elif isinstance(obj, list):
        for n, k in enumerate(obj):
            obj[n]=jstr_substitute(k, wfs)
    elif isinstance(obj, str) or isinstance(obj, unicode):
        if obj in wfs.keys() and obj != '#main':
            obj=wfs[obj]
    else:
        print('Warning: unexpected type: ')
        print(type(obj))
    return obj


def jstr_transform_content2(json_content):
    if "$graph" not in json_content:
        return json_content
    wfs = jstr_get_wfs(json_content)
    json_content_new = jstr_substitute(wfs['#main'],wfs)
    json_content_new['cwlVersion'] = json_content['cwlVersion']
    #for key in json_content.keys():
    #    if key == 'cwlVersion':
    #        json_content_new[key] = json_content[key]
    #print(json_content_new)
    return json_content_new
