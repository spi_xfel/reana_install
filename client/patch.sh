#!/bin/bash

#find python package 'reana-client'; add utils4cwl.py file; patch utils.py to use them  
#NEED: 'patch' command

mpath=$(python -m pip show reana-client|grep Location|cut -f 2 -d\ )

[ ! -d $mpath ] && echo "Cannot find reana-client" && exit 1
mpath=$mpath/reana_client

chmod 644 utils4cwl.py
cp utils4cwl.py utils.py.patch $mpath/
cd $mpath
patch < utils.py.patch
rm -f utils.py.patch
cd -
